//CRUD Operations
/*
	Create, Read, update and delete
*/

/*
	CREATE - allows us to create a document under a document or create a collection if it does not exist yet. 

	syntax: 
	db.collections.insertOne()
		-collection = stands for collection name
*/

db.users.insertOne(
		{
			"firstName" : "Tony",
			"lastName" : "Stark",
			"username" : "iAmIronMan",
			"email" : "iloveyou3000@mail.com",
			"password": "starkIndustries",
			"isAdmin" : true
		}

	)

// db.collection.insertMany() = to insert/ create more documents

db.users.insertMany([
	{
		"firstName" : "Pepper",
		"lastName" : "Potts",
		"username" : "rescueArmor",
		"email" : "pepper@mail.com",
		"password": "whereIsTonyAgain",
		"isAdmin" : false
	},
	{
		"firstName" : "Steve",
		"lastName" : "Rogers",
		"username" : "theCaptain",
		"email" : "captAmerica@mail.com",
		"password": "iCanLiftMjolnirtoo",
		"isAdmin" : false
	},
	{
		"firstName" : "Thor",
		"lastName" : "Odinson",
		"username" : "mightyThor",
		"email" : "ThorNotLoki@mail.com",
		"password": "iAmWorthyToo",
		"isAdmin" : false
	},
	{
		"firstName" : "Loki",
		"lastName" : "Odinson",
		"username" : "godOfMischief",
		"email" : "loki@mail.com",
		"password": "iAmReallyLoki",
		"isAdmin" : false
	}


	])


//MiniActivity 

db.courses.insertMany([
	{
		"name": "Javascript",
		"price": 3500,
		"description": "Learn Javascript in a week!",
		"isActive": true
	},
	{
		"name": "HTML",
		"price": 1000,
		"description": "Learn Basic HTML in 3 days!",
		"isActive": true
	},
	{
		"name": "CSS",
		"price": 2000,
		"description": "Make your website fancy, learn CSS now!",
		"isActive": true
	}


	])

/*
	READ - allows us to retrieve data
		- it needs a query or filters to specify the document we are retrieving.

		syntax: 
		db.collection.find() = allows us to retrieve ALL documents in the collections.

*/

db.users.find()

// sytnax: db.collections.findOne({criteria: value}) = allows us to find the document that matches our criteria

db.users.find({"isAdmin": false});
db.users.find({});

//find with 2 criteria
db.users.find({"lastName" : "Odinson", "firstName": "Loki"});

/*
	Update - allows us to update documents.
			-  allows us to criteria or filter
			- $set operator
			- REMINDER: Updates are permanent and can't be rolled back. 

			syntax: db.collections.updateOne({}, {$set: {}});
				-allows us to update one document that first satisfy the criteria
*/

db.users.updateOne({lastName: "Potts"}, {$set: {lastName: "Stark"}});

//db.collections.updateMany({criteria: value}, {$set: {fieldToBeUpdated : updatedValue}}) = allows us to update all document that satisfy the criteria
db.users.updateMany({lastName: "Odinson"}, {$set: {isAdmin: true}});

// syntax: db.collections.update({},{$set: {fieldToBeUpdated: updatedValue}});
// update the first item in the collection

db.users.update({},{$set: {email : "starkIndustries@mail.com"}});


//mini activity


db.courses.find();
db.courses.updateMany({}, {$set: {"enrollees": 10}});

/*
	DELETE = allows us to delete documents
		= provide criteria and filter to specify which document to delete from the collection.
		= REMINDER: its hard to retrieve data once its deleted

		syntax: db.collections.deleteOne({criteria : value})
		 allows us to delete the first item that matches our criteria
*/

db.users.deleteOne({isAdmin:false})

//db.collections.deleteMany({criteria : value})
	//allow us to delete all items that fits the criteria

	db.users.deleteMany({lastName:"Odinson"})

//db.collections.deleteOne({}) = allows to delete the first document
	db.users.deleteOne({})

//db.collections.deleteMany();
//deletes all the items in the collections

db.users.deleteMany();