//Hotel Datebase | rooms

//insert one room
db.rooms.insertOne(
	{
		"name": "single",
		"accomodates": 2,
		"price": 1000,
		"description": "A simple room with all the basic necessities.",
		"roomsAvailable": 10,
		"isAvailable": false
	}
)

//insert multiple rooms
db.rooms.insertMany([
	{
		"name": "double",
		"accomodates": 3,
		"price": 2000,
		"description": "A room fit for a small family going on a vacation",
		"roomsAvailable": 5,
		"isAvailable": false
	},
	{
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room with a queen sozed bed perfect for a simple getaway",
		"roomsAvailable": 15,
		"isAvailable": false
	},


])
db.rooms.find({});

//find room : double
db.rooms.find({name: "double"});


//update queen room to zero available rooms
db.rooms.updateOne({roomsAvailable: 15}, {$set : {roomsAvailable : 0}});

//delete rooms with zero availability
db.rooms.deleteMany({roomsAvailable: 0});